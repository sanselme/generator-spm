<% if (kitura) { %>import Kitura
import KituraCORS
import HeliumLogger
import SwiftyJSON

HeliumLogger.use()

let router = Router()
router.all(middleware: [BodyParser(), CORS(options: Options())])

router.get { _, response, next in
  defer { next() }

  response.status(.accepted).send(json: JSON(["message": "Hello Demo"]))
}

Kitura.addHTTPServer(onPort: 8090, with: router)
Kitura.run()<% } else { %>print("Hello <%= name %>")<% } %>
