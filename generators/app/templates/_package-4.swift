// swift-tools-version:4.0
import PackageDescription

let urls: [String] = []

let targets: [[String: Any]] = [
  ["name": "<%= name %>"]
]

let package = Package(
    name: "<%= name %>",
    dependencies: urls.map { .package(url: $0, from: "1.0.0") },
    targets: targets.flatMap { target in
      guard let name = target["name"] else { return .target(name: "") }
      guard let dependencies = target["dependencies"] else { return .target(name: name as! String) }

      return .target(name: name as! String, dependencies: (dependencies as! [String]).map { Target.Dependency(stringLiteral: $0) })
    }
)
