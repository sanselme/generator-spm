// swift-tools-version:3.0
import PackageDescription

let urls: [String] = [<% if (kitura) { %>
  "https://github.com/ibm-swift/kitura-cors",
  "https://github.com/ibm-swift/heliumlogger"
<% } %>]

let targets: [[String: Any]] = [
  ["name": "<%= name %>"<% if (helpers) { %>, "dependencies": ["SwiftHelpers"]],
  ["name": "SwiftHelpers"]<% } else { %>]<% } %>
]

let package = Package(
    name: "<%= name %>",
    targets: targets.flatMap { target in
      guard let name = target["name"] else { return Target(name: "") }
      guard let dependencies = target["dependencies"] else { return Target(name: name as! String) }

      return Target(name: name as! String, dependencies: (dependencies as! [String]).map { Target.Dependency(stringLiteral: $0) })
    },
    dependencies: urls.map { .Package(url: $0, versions: Version(.min, .min, .min) ..< Version(.max, .max, .max)) }
)
