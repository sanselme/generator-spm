import SwiftyJSON

public typealias NextHandler = () -> Void

public protocol JSONSerializable {
  var data: JSON { get }
  func toJSON() -> JSON
}

public protocol Repository {
  func get(tablename: String, id: String?, predicate: String?) throws -> JSON
  func save(tablename: String, id: String?, data: JSON?) throws -> JSON
  func destroy(tablename: String, id: String?) throws -> JSON
}

extension JSONSerializable {
  public func toJSON() -> JSON { return data }
}

extension Sequence where Iterator.Element: JSONSerializable {
  public func toJSON() -> JSON { return JSON(self.map { $0.data }) }
}
