"use strict";

const _ = require("lodash");
const chalk = require("chalk");
const Generator = require("yeoman-generator");
const mkdir = require("mkdirp");
const yosay = require("yosay");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(yosay(`Welcome to the extraordinary ${chalk.red("generator-spm")} generator!`));

    const prompts = [{
      type: "input",
      name: "name",
      message: "Project name",
      default: _.kebabCase(this.appname)
    },{
      type: "input",
      name: "description",
      message: "Description",
      default: answers => _.startCase(answers.name)
    },{
      type: "input",
      name: "author",
      message: "Your name",
      default: this.user.git.name()
    },{
      type: "input",
      name: "email",
      message: "Your email address",
      default: this.user.git.email()
    },{
      type: "rawlist",
      name: "package",
      message: "Package type",
      choices: [{
        name: "executable",
        value: "executable",
        checked: true
      },{
        name: "framework",
        value: "framework",
        checked: false
      }]
    },{
      type: "rawlist",
      name: "version",
      message: "Swift version",
      choices: [{
        name: "v3",
        value: "v3",
        checked: true
      },{
        name: "v4",
        value: "v4",
        checked: false
      }]
    },{
      type: "confirm",
      name: "helpers",
      message: "Include SwiftHelpers",
      default: true,
      when: answers => answers.version === "v3"
    },{
      type: "confirm",
      name: "kitura",
      message: "Include Kitura",
      default: true,
      when: answers => answers.version === "v3"
    }];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
      this.props.name = _.startCase(this.props.name);
      this.props.author = _.startCase(this.props.author);
      this.props.year = ((new Date).toISOString().split("T")[0]).split("-")[0];
    });
  }

  writing() {
    const context = this.props.context = {
      name: this.props.name,
      author: this.props.author,
      email: this.props.email,
      description: this.props.description,
      year: this.props.year,
      helpers: this.props.helpers,
      kitura: this.props.kitura
    };

    mkdir(this.destinationRoot(this.props.name));
    mkdir(this.destinationPath("Tests"));
    mkdir(this.destinationPath("Sources"));
    mkdir(this.destinationPath("Sources", this.props.name));

    this.fs.copy(this.templatePath("_editorconfig"), this.destinationPath(".editorconfig"));
    this.fs.copy(this.templatePath("_gitignore"), this.destinationPath(".gitignore"));

    this.fs.copyTpl(this.templatePath("LICENSE.txt"), this.destinationPath("LICENSE.txt"), context);
    this.fs.copyTpl(this.templatePath("README.md"), this.destinationPath("README.md"), context);

    if (this.props.package === "executable") {
      this.fs.copyTpl(this.templatePath("main.swift"), this.destinationPath("Sources", this.props.name, "main.swift"), context);
    } else if (this.props.package === "framework") {
      this.fs.copyTpl(this.templatePath("main.swift"), this.destinationPath("Sources", this.props.name, `${this.props.name}.swift`), context);
    }

    if (this.props.version === "v3") {
      this.fs.copyTpl(this.templatePath("_package-3.swift"), this.destinationPath("Package.swift"), context);
    } else if (this.props.version === "v4") {
      this.fs.copyTpl(this.templatePath("_package-4.swift"), this.destinationPath("Package.swift"), context);
    }

    if (this.props.helpers) {
      mkdir(this.destinationPath("Sources", "SwiftHelpers"));
      this.fs.copy(this.templatePath("SwiftHelpers.swift"), this.destinationPath("Sources", "SwiftHelpers", "SwiftHelpers.swift"));
    }
  }

  install() {
    this.spawnCommandSync("swift", ["package", "update"]);
    this.spawnCommandSync("swift", ["package", "generate-xcodeproj"]);
    this.spawnCommandSync("open", [this.destinationPath(`${this.props.name}.xcodeproj`)]);
  }
};
