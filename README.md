# Swift Project Generator

# generator-spm [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

> Swift Generator

## Installation

First, install [Yeoman](http://yeoman.io) and generator-spm using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-spm
```

Then generate your new project:

```bash
yo spm
```

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

--------------------------------------------------------------------------------

The MIT License (MIT)

Copyright © 2017 Schubert Anselme

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[coveralls-image]: https://coveralls.io/repos/sanselme/generator-spm/badge.svg
[coveralls-url]: https://coveralls.io/r/sanselme/generator-spm
[daviddm-image]: https://david-dm.org/sanselme/generator-spm.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/sanselme/generator-spm
[npm-image]: https://badge.fury.io/js/generator-spm.svg
[npm-url]: https://npmjs.org/package/generator-spm
[travis-image]: https://travis-ci.org/sanselme/generator-spm.svg?branch=master
[travis-url]: https://travis-ci.org/sanselme/generator-spm
