"use strict";

var path = require("path");
var assert = require("yeoman-assert");
var helpers = require("yeoman-test");

describe("generator-spm:app", () => {
  beforeAll(() => helpers.run(path.join(__dirname, "../generators/app")).withPrompts({}));

  it("creates files", () => {
    assert.file([
      ".editorconfig",
      ".gitignore",
      "LICENSE.txt",
      "Package.swift",
      "README.md",
    ]);
  });
});
